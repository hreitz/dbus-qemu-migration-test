use futures::executor::block_on;
use std::convert::TryInto;
use std::str::FromStr;

use zbus::dbus_interface;
use zbus::fdo::RequestNameFlags;

struct VMState {
    id: String,
    state: String,
}

#[dbus_interface(name = "org.qemu.VMState1")]
impl VMState {
    #[dbus_interface(property, name = "Id")]
    fn id(&self) -> &str {
        &self.id
    }

    fn load(&mut self, state: &[u8]) {
        self.state = String::from_utf8_lossy(state).into();
        println!("[{}] Loaded state: {}", self.id, self.state);
    }

    fn save(&self) -> &[u8] {
        println!("[{}] Saving state", self.id);
        self.state.as_bytes()
    }
}

fn setup_dbus(bus: String, id: String) -> zbus::Result<zbus::Connection> {
    let con = block_on(async {
        zbus::ConnectionBuilder::address(zbus::Address::from_str(&bus)?)?
            .build()
            .await
    })?;

    let state = format!("Some initial state for {}", id);
    let vmstate = VMState { id, state };

    let service_name = "org.qemu.VMState1".try_into()?;

    block_on(async {
        con.object_server_mut()
            .await
            .at("/org/qemu/VMState1", vmstate)?;

        zbus::fdo::DBusProxy::new(&con)
            .await?
            .request_name(
                service_name,
                RequestNameFlags::AllowReplacement.into(),
            )
            .await
    })?;

    Ok(con)
}

fn main() {
    let mut args = std::env::args().skip(1);

    let bus = args
        .next()
        .unwrap_or_else(|| std::env::var("DBUS_SESSION_BUS_ADDRESS").unwrap());

    let id = args
        .next()
        .unwrap_or_else(|| String::from("dbus-qemu-migration-test"));

    let _con = setup_dbus(bus, id).unwrap();

    std::thread::sleep(std::time::Duration::from_secs(60));
}
