#!/bin/sh

function status() {
    echo -e "\033[1m$@\033[0m"
}

dbus_pidfile="$PWD/qemu-dbus.pid"
dbus_sock="$PWD/qemu-dbus.sock"
dbus_addr="unix:path=$dbus_sock"
dbusd_conf="$PWD/qemu-dbusd.conf"
mig_file="$PWD/qemu.mig"

status 'Setting up dbus...'

cat > "$dbusd_conf" <<EOF
<!DOCTYPE busconfig PUBLIC "-//freedesktop//DTD D-Bus Bus Configuration 1.0//EN"
 "http://www.freedesktop.org/standards/dbus/1.0/busconfig.dtd">
<busconfig>
    <listen>$dbus_addr</listen>
    <fork />
    <pidfile>$dbus_pidfile</pidfile>

    <policy user="$(id -u)">
        <allow send_type="*" />
        <allow receive_type="*" />
        <allow own="org.qemu.VMState1" />
    </policy>
</busconfig>
EOF

dbus-daemon "--config-file=$dbusd_conf"


status 'Connecting two simulated vhost-user processes...'

id_a=vhost-proc-A
id_b=vhost-proc-B

cargo build || exit 1

cargo run -q "$dbus_addr" $id_a &
cargo run -q "$dbus_addr" $id_b &


status 'Launching qemu, and migrating to file...'

(sleep 2; echo "migrate \"exec:cat > $mig_file\""; echo 'quit') | \
    qemu-system-x86_64 \
        -object "dbus-vmstate,id=vhost-vmstate,addr=$dbus_addr,id-list=$id_a,,$id_b" \
        -monitor stdio


status 'Resuming qemu from file...'

(sleep 2; echo 'quit') | \
    qemu-system-x86_64 \
        -object "dbus-vmstate,id=vhost-vmstate,addr=$dbus_addr,id-list=$id_a,,$id_b" \
        -incoming "exec:cat $mig_file" \
        -monitor stdio


status 'Cleaning up'

kill %1
kill %2
kill $(cat "$dbus_pidfile")

wait
wait
wait

rm "$dbusd_conf"
rm "$mig_file"
