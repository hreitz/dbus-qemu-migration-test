# dbus-qemu-migration-test

This is a very simple test of qemu’s *dbus-vmstate* object, which allows
external processes (like vhost-user daemons) to save data to and load
data from qemu’s migration stream.

The Rust program sets up DBus server providing the `org.qemu.VMState1`
interface with very simple `Load` and `Save` methods.

The *test.sh* script launches two instances of this program on a local
DBus, then attaches a qemu instance to it, which migrates to and from a
file.
